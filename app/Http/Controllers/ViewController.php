<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class ViewController extends Controller
{
    public function ViewPage()
    {
        return view('View');
    }

    public function GetAllData()
    {
        $AllTheData = News::all();
        return $AllTheData;
    }

    public function UpdateData(Request $request)
    {
        $UpdateData = News::find($request->get('NewsId'));
        $UpdateData->news_headline = $request->get('NewsHeadline');
        $UpdateData->email = $request->get('Email');
        $UpdateData->news_content = $request->get('NewsContent');
        $UpdateData->update();

        return 1;
    }

    public function DeleteData(Request $request)
    {
        $DeleteAll = News::where('id',$request->input('id'))->get()->first();
        $DeleteAll->delete();

        return 1;
    }
}
