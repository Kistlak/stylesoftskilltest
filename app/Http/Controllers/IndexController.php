<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class IndexController extends Controller
{
    public function IndexPage()
    {
        return view('index');
    }

    public function InsertNews(Request $request)
    {
        $InsertNews = new News;
        $InsertNews->news_headline = $request->get('NewsHeadline');
        $InsertNews->email = $request->get('Email');
        $InsertNews->news_content = $request->get('NewsContent');
        $InsertNews->save();

        $request->session()->flash('Msg', 'Successfully Inserted !!');
        return redirect()->back();
    }
}
