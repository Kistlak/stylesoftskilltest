-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 18, 2019 at 11:16 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ss_skill_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_headline` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `news_content` longtext,
  `created_at` varchar(250) NOT NULL,
  `updated_at` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_headline`, `email`, `news_content`, `created_at`, `updated_at`) VALUES
(1, 'NewsOneUpdate', 'kistlakall@gmail.comUpdate', 'ContentrrUpdate', '2019-08-18 08:18:01', '2019-08-18 11:07:35'),
(2, 'Just Updated', 'kistlakall@gmail.comTest', 'Check', '2019-08-18 08:32:32', '2019-08-18 11:09:37'),
(10, 'BBC News', 'bbc@cnn.com', 'here goes the news bbc', '2019-08-18 11:11:32', '2019-08-18 11:11:32'),
(5, 'NewsTwo', 'newstwo@yahoo.com', 'here goes the news 2', '2019-08-18 11:08:16', '2019-08-18 11:08:16'),
(6, 'NewsThre', 'lak@sl.com', 'here goes the news 3', '2019-08-18 11:08:44', '2019-08-18 11:08:44'),
(9, 'Go Just Go', 'kistlakall@gmail.com', 'here goes the news', '2019-08-18 11:11:10', '2019-08-18 11:11:10'),
(8, 'News Check', 'justcheck@check.com', 'check the news here', '2019-08-18 11:10:01', '2019-08-18 11:10:01');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
