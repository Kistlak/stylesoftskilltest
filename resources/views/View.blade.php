@extends('Templates.IndexTemplate')

@section('content')

    <link href="https://codeseven.github.io/toastr/build/toastr.min.css" rel="stylesheet" type="text/css" />

    <div class="col-md-12 col-sm-12 hero-feature"> <!-- Start Of The Col Class -->

        <table id="employee" class="table table-bordered table table-hover" cellspacing="0" width="100%">
            <colgroup><col><col><col></colgroup>
            <thead class="thead-dark">
            <tr>
                <th>News Id</th>
                <th>News Name</th>
                <th>Email</th>
                <th>News Content</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody id="emp_body">
            </tbody>
        </table>
        <div id="pager">
            <ul id="pagination" class="pagination-sm"></ul>
        </div>

    </div> <!-- End Of The Col Class -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="http://josecebe.github.io/twbs-pagination/js/jquery.twbsPagination.js" type="text/javascript"></script>
    <script src="https://codeseven.github.io/toastr/build/toastr.min.js" type="text/javascript"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var $pagination = $('#pagination'),
                totalRecords = 0,
                records = [],
                displayRecords = [],
                recPerPage = 5,
                page = 1,
                totalPages = 0;
            $.ajax({
                url: "{{route('GetAllData')}}",
                async: true,
                dataType: 'json',
                success: function (data) {
                    records = data;
                    console.log(records);
                    totalRecords = records.length;
                    totalPages = Math.ceil(totalRecords / recPerPage);
                    apply_pagination();
                }
            });

            function generate_table() {
                var tr;
                $('#emp_body').html('');
                for (var i = 0; i < displayRecords.length; i++) {
                    tr = $('<tr class="TblTr"/>');
                    tr.append("<td>" + displayRecords[i].id + "</td>");
                    tr.append("<td class='NewsdHeadline'>" + displayRecords[i].news_headline + "</td>");
                    tr.append("<td class='Email'>" + displayRecords[i].email + "</td>");
                    tr.append("<td class='NewsContent'>" + displayRecords[i].news_content + "</td>");
                    tr.append('<td><button type="button" class="btn btn-success EditBtn" data-toggle="modal" data-target="#EdiModal_'+[i]+'">Edit</button></td>');
                    tr.append('<td><button type="button" class="btn btn-danger DeleteBtn">Delete</button><input type="hidden" id="NewsId" value="'+ displayRecords[i].id +'"></td>');

                    tr.append('<!-- Modal -->' +
                        '    <div class="modal fade UpdateModal" id="EdiModal_'+[i]+'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                        '        <div class="modal-dialog" role="document">' +
                        '            <div class="modal-content">' +
                        '                <div class="modal-header">' +
                        '                    <h5 class="modal-title" id="exampleModalLabel">Edit '+ displayRecords[i].news_headline +'</h5>' +
                        '                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                        '                        <span aria-hidden="true">&times;</span>' +
                        '                    </button>' +
                        '                </div>' +
                        '                <div class="modal-body">' +
                        '                     <form class="needs-validation" id="NewsForm" novalidate>' +
                        '            {{ csrf_field() }}' +
                        '' +
                        '            <div class="form-row">' +
                        '                <div class="col-md-6 mb-3">' +
                        '                    <label for="validationCustom01">News Headline</label>' +
                        '                    <input type="text" class="form-control" id="validationCustom01" name="NewsHeadline" value="'+ displayRecords[i].news_headline +'" required>' +
                        '                    <div class="valid-feedback">' +
                        '                        Looks good!' +
                        '                    </div>' +
                        '                    <div class="invalid-feedback">' +
                        '                        Please enter a news headline' +
                        '                    </div>' +
                        '                </div>' +
                        '                <div class="col-md-6 mb-3">' +
                        '                    <label for="validationCustom02">Email</label>' +
                        '                    <input type="email" class="form-control" id="validationCustom02" name="Email" value="'+ displayRecords[i].email +'" required>' +
                        '                    <input type="hidden" class="form-control" id="validationCustom03" name="Email" value="'+ displayRecords[i].news_headline +'" required>' +
                        '                    <div class="valid-feedback">' +
                        '                        Looks good!' +
                        '                    </div>' +
                        '                    <div class="invalid-feedback">' +
                        '                        Please enter valid email' +
                        '                    </div>' +
                        '                </div>' +
                        '' +
                        '            </div>' +
                        '' +
                        '            <div class="form-row">' +
                        '                <div class="col-md-12 mb-6">' +
                        '                    <div class="form-group">' +
                        '                        <label for="exampleFormControlTextarea1">News Content</label>' +
                        '                        <input type="text" class="form-control" id="exampleFormControlTextarea1" name="NewsContent" value="'+ displayRecords[i].news_content +'" required>' +
                        '                        <div class="invalid-feedback">' +
                        '                            Please provide news content.' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>' +
                        '' +
                        '            </div>' +
                        '' +
                        '                </div>' +
                        '                <div class="modal-footer">' +
                        '                    <input type="hidden" name="NewsId" id="NewsId" value="'+ displayRecords[i].id +'">' +
                        '                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                        '            <button type="button" class="btn btn-primary UpdateBtn" id="UpdateBtn">Update</button>' +
                        '        </form>' +
                        '                </div>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>');

                    $('#emp_body').append(tr);

                }
            }

            function apply_pagination() {
                $pagination.twbsPagination({
                    totalPages: totalPages,
                    visiblePages: 6,
                    onPageClick: function (event, page) {
                        displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
                        endRec = (displayRecordsIndex) + recPerPage;

                        displayRecords = records.slice(displayRecordsIndex, endRec);
                        generate_table();
                    }
                });
            }

            $(document).on('click',".UpdateBtn", function(){

                var NewsId = $(this).closest('.UpdateModal').find('#NewsId').val();
                var NewsHeadline = $(this).closest('.UpdateModal').find('#validationCustom01').val();
                var Email = $(this).closest('.UpdateModal').find('#validationCustom02').val();
                var NewsContent = $(this).closest('.UpdateModal').find('#exampleFormControlTextarea1').val();

                var ThisNewsHeadline = $(this).closest('.TblTr').find('.NewsdHeadline');
                var ThisEmail = $(this).closest('.TblTr').find('.Email');
                var ThisNewsContent = $(this).closest('.TblTr').find('.NewsContent');

                $.ajax({
                url: "{{route('UpdateData')}}",
                    data: {
                        NewsId: NewsId,
                        NewsHeadline: NewsHeadline,
                        Email: Email,
                        NewsContent: NewsContent,
                        _token: '{{csrf_token()}}'
                    },
                method: 'POST',
                success: function (data) {

                    if(data == "1")
                    {
                        ThisNewsHeadline.html(NewsHeadline);
                        ThisEmail.html(Email);
                        ThisNewsContent.html(NewsContent);
                        $('.UpdateModal').modal('hide');

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }

                        Command: toastr["success"]("Successfully Updated !!")
                    }
                }
                });

            });

            $(document).on('click',".DeleteBtn", function(){

                var NewsId = $(this).closest('.TblTr').find('#NewsId').val();

                    var ThisSaveBtn = $(this).closest("tr");

                    $.ajax({
                        url: "{{route('DeleteData')}}",
                        data: {
                            id: NewsId,
                            _token: '{{csrf_token()}}'
                        },
                        method: 'POST',

                        success: function (data) {
                            ThisSaveBtn.remove();

                            if(data == "1")
                            {
                                $('.UpdateModal').modal('hide');

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }

                                Command: toastr["success"]("Successfully Deleted !!")
                            }
                        }
                    });
            });
        });
    </script>

@endsection