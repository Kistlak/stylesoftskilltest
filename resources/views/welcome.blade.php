    <div class="modal fade" id="EdiModal_[i]" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document"> 
                    <div class="modal-content"> 
                            <div class="modal-header"> 
                                    <h5 class="modal-title" id="exampleModalLabel">Edit  displayRecords[i].news_headline </h5> 
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                                            <span aria-hidden="true">&times;</span> 
                                        </button> 
                                </div> 
                            <div class="modal-body"> 
                                     <form class="needs-validation" id="NewsForm" novalidate> 
                     
                                {{ csrf_field() }} 
                     
                                <div class="form-row"> 
                                        <div class="col-md-6 mb-3"> 
                                                <label for="validationCustom01">News Headline</label> 
                                                <input type="text" class="form-control" id="validationCustom01" name="NewsHeadline" value=" displayRecords[i].news_headline " required> 
                                                <div class="valid-feedback"> 
                                                        Looks good! 
                                                    </div> 
                                                <div class="invalid-feedback"> 
                                                        Please enter a news headline 
                                                    </div> 
                                            </div> 
                                        <div class="col-md-6 mb-3"> 
                                                <label for="validationCustom02">Email</label> 
                                                <input type="email" class="form-control" id="validationCustom02" name="Email" value=" displayRecords[i].email " required> 
                                                <div class="valid-feedback"> 
                                                        Looks good! 
                                                    </div> 
                                                <div class="invalid-feedback"> 
                                                        Please enter valid email 
                                                    </div> 
                                            </div> 
                         
                                    </div> 
                     
                                <div class="form-row"> 
                                        <div class="col-md-12 mb-6"> 
                                                <div class="form-group"> 
                                                        <label for="exampleFormControlTextarea1">News Content</label> 
                                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="NewsContent" required> displayRecords[i].news_content </textarea> 
                                                        <div class="invalid-feedback"> 
                                                                Please provide news content. 
                                                            </div> 
                                                    </div> 
                                            </div> 
                         
                                    </div> 
                     
                                    </div> 
                            <div class="modal-footer"> 
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                            <button type="button" class="btn btn-primary UpdateBtn" id="UpdateBtn" onclick="UpdateFunc()">Update</button> 
                        </form> 
                                </div> 
                        </div> 
                </div> 
        </div>