@extends('Templates.IndexTemplate')

@section('content')

    <div class="col-md-12 col-sm-12 hero-feature"> <!-- Start Of The Col Class -->

        @if(session()->has('Msg'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>{{ session()->get('Msg') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <form class="needs-validation" id="NewsForm" action="{{route('InsertNews')}}" method="POST" novalidate>

            {{ csrf_field() }}

            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationCustom01">News Headline</label>
                    <input type="text" class="form-control" id="validationCustom01" name="NewsHeadline" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                    <div class="invalid-feedback">
                        Please enter a news headline
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationCustom02">Email</label>
                    <input type="email" class="form-control" id="validationCustom02" name="Email" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                    <div class="invalid-feedback">
                        Please enter valid email
                    </div>
                </div>

            </div>

            <div class="form-row">
                <div class="col-md-12 mb-6">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">News Content</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="NewsContent" required></textarea>
                        <div class="invalid-feedback">
                            Please provide news content
                        </div>
                    </div>
                </div>

            </div>

            <button class="btn btn-primary" type="submit">Insert</button>
        </form>

    </div> <!-- End Of The Col Class -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

@endsection