<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img\Logo\WisdomIcon.jpg">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <title>Skill Test</title>

</head>

@include('Includes.NavBar')
<body>

<div class="container"> <!-- Start Of The Container Class -->

    <div class="row text-center"> <!-- Start Of The Row Class -->

        @yield('content')

    </div> <!-- End Of The Row Class -->

</div> <!-- End Of The Container Class -->

</body>
</html>