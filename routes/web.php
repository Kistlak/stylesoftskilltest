<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','IndexController@IndexPage')->name('Index');

Route::post('InsertNews','IndexController@InsertNews')->name('InsertNews');

Route::get('View','ViewController@ViewPage')->name('View');

Route::get('GetAllData','ViewController@GetAllData')->name('GetAllData');

Route::post('UpdateData','ViewController@UpdateData')->name('UpdateData');

Route::post('DeleteData','ViewController@DeleteData')->name('DeleteData');
